from django.shortcuts import render
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required
#Import YOUR VIEW first !  

'''
Create a view that will get all of 
the instances of the Receipt model and put 
them in the context for the template.
'''


'''
Protect the list view for 
the Receipt model so that
 only a person who has logged in can access it.
'''

# Make a view function 
# Create your views here.
@login_required
def show_receipts(request):
 
                            # F - 8 Change the queryset of the view to 
                            # filter the Receipt objects where purchaser
                            #  equals the logged in user.
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = { 
        "receipts":receipts, 
    }

    return render(request,"receipts/receipts.html" ,context  )