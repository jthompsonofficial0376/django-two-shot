in a terminal first  

 cd ..

cd hack-reactor
 cd projects
 cd django-two-shot


# create python .venv  
 - python -m venv .venv

#activate .venv
./.venv/Scripts/Activate.ps1


pip install black flake8 djlint django

--reactivate  

#pip freeze a requirements.txt file 
 pip freeze > requirements.txt

#start a project  

django-admin startproject expenses .

#run migrations

python manage.py makemigrations 
python manage.py migrate 


# Create a superuser if you want to access the admin 











