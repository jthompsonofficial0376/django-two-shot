from django.shortcuts import render, redirect
from .forms import UserForm

from django.contrib.auth import authenticate, login


#Views , continue to help us set up a display for our models ,  

def account_login(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            
            user = authenticate(
                request,
                username=username,
                password=password,
            )


            if user is not None:
                login(request, user)

            # Process the form data - and LOG IN )
                return redirect('home')
            
    else:
        form = UserForm()
    return render(request, 'accounts/login.html', {'form': form})
    



# this takes the name of the path, like ,   name=" "     in the apps url file