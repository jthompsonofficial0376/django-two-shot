from django.forms import ModelForm
from receipts.models import Receipt

from django import forms

class UserForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput())

